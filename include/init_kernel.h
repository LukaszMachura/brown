#ifndef __INITKERNEL_H__
#define __INITKERNEL_H__

//#define _GNU_SOURCE
#include <math.h>

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "cudaSafe.h"
#include "rng.h"

#ifdef __cplusplus
extern "C" {
#endif

void init(device_t (*devs)[DEVS]);

#ifdef __cplusplus
}
#endif

#endif //__INITKERNEL_H__
