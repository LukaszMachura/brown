#ifndef __ALGORITHM_H__
#define __ALGORITHM_H__

#include <math.h>
#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"

extern __device__ void makeStep(int *pcd, curandState * const state, float *x, float *v,
                oscillator_t *w, const params_t *p);

#endif //__ALGORITHM_H__
