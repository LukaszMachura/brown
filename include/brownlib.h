#ifndef __BROWNLIB_H__
#define __BROWNLIB_H__

#include "brown_types.h"

void devsRestore(const device_t (*devsHost)[DEVS], device_t (*devs)[DEVS]);
void devsFree(device_t (*devs)[DEVS]);

void constantsInit(params_t *p);
void constantsPin(params_t *p);
void constantsUnpin(params_t *p);
void devsSynchronize();
void constantsMemcpy(const device_t (*devs)[DEVS], params_t *p);
void streamSetParams(stream_t *stream, const ulong paths);

void hostAlloc(host_t *h, const ulong paths, const int mode);
void hostFree(host_t *h);
void devsHostAlloc(device_t (*devs)[DEVS], const ulong paths);
void devsBackup(const device_t (*devs)[DEVS], device_t (*devsHost)[DEVS]);
void devsFreeHost(device_t (*devs)[DEVS]);

void init(device_t (*devs)[DEVS]);

void streamAlloc(stream_t *stream, const ulong paths, const int mode);
void devsAlloc(device_t (*devs)[DEVS], const ulong paths, const int mode);

void momentsRun(device_t (*dev)[DEVS]);
void momentsAlloc(stream_t *stream);
void momentsMemcpy(const device_t (*devs)[DEVS], host_t *h, float *sv, float *sv2, float *d);

void rngInit(device_t (*devs)[DEVS]);
void seedsGen(host_t *host, const ulong paths);
void seedsMemcpy(const host_t *h, device_t (*devs)[DEVS]);
void seedsAlloc(stream_t *stream);
void seedsFreeHost(host_t *h);

void trajRun(device_t (*devs)[DEVS]);

void trajAlloc(stream_t *stream);
void trajMemcpy(const device_t (*devs)[DEVS], host_t *h);


#endif
