#ifndef __HOST_H__
#define __HOST_H__

#include <curand.h>
#include <curand_kernel.h>

#include "help_functions.h"
#include "brown_types.h"
#include "cudaSafe.h"

#ifdef __cplusplus
extern "C" {
#endif

void hostAlloc(host_t *h, const ulong paths, const int mode);
void hostFree(host_t *h);
void devsHostAlloc(device_t (*devs)[DEVS], const ulong paths);
void devsBackup(const device_t (*devs)[DEVS], device_t (*devsHost)[DEVS]);
void devsFreeHost(device_t (*devs)[DEVS]);

#ifdef __cplusplus
}
#endif

#endif
