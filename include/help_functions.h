#ifndef __HELPFUNCTIONS_H__
#define __HELPFUNCTIONS_H__

#include <math.h>

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "cudaSafe.h"

#ifdef __cplusplus
extern "C" {
#endif

void constantsInit(params_t *p);
void constantsPin(params_t *p);
void constantsUnpin(params_t *p);
void devsSynchronize();
void constantsMemcpy(const device_t (*devs)[DEVS], params_t *p);
void streamSetParams(stream_t *stream, const ulong paths);

#ifdef __cplusplus
}
#endif

#endif
