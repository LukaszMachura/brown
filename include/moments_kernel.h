#ifndef __MOMENTSKERNEL_H__
#define __MOMENTSKERNEL_H__

//#define _GNU_SOURCE
#include <math.h>

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "algorithm.h"
#include "cudaSafe.h"

#ifdef __cplusplus
extern "C" {
#endif

void momentsRun(device_t (*dev)[DEVS]);

#ifdef __cplusplus
}
#endif

#endif //__MOMENTSKERNEL_H__
