#ifndef __TRAJECTORY_H__
#define __TRAJECTORY_H__

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "cudaSafe.h"

#ifdef __cplusplus
extern "C" {
#endif

void trajAlloc(stream_t *stream);
void trajMemcpy(const device_t (*devs)[DEVS], host_t *h);

#ifdef __cplusplus
}
#endif

#endif
