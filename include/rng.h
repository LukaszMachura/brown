#ifndef __RNG_H__
#define __RNG_H__

#include <time.h>
#include <curand.h>
#include <curand_kernel.h>

#include "cudaSafe.h"
#include "brown_types.h"

#ifdef __cplusplus
extern "C" {
#endif

void rngInit(device_t (*devs)[DEVS]);
void seedsGen(host_t *host, const ulong paths);
void seedsMemcpy(const host_t *h, device_t (*devs)[DEVS]);
void seedsAlloc(stream_t *stream);
void seedsFreeHost(host_t *h);


#ifdef __cplusplus
}
#endif

#endif //__RNG_H__
