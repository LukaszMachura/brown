#ifndef __BROWN_H__
#define __BROWN_H__
#include <stdbool.h>
#include "defs.h"

#define PI 3.14159265358979f
#define INIT 1
#define TRAJECTORY 2
#define MOMENTS 4

#ifdef DEBUG
#define print(str, ...) printf(str, ##__VA_ARGS__);
#else
#define print(...)
#endif

#ifdef __BROWNLIB_H__
#include <cuda_runtime.h>

struct curandStateXORWOW {
    unsigned int d, v[5];
    int boxmuller_flag;
    int boxmuller_flag_double;
    float boxmuller_extra;
    double boxmuller_extra_double;
};


typedef struct curandStateXORWOW curandState;
#endif

typedef unsigned long ulong;

typedef struct oscillatorStruct{
  float cost; //cos(wt)
  float sint; //sin(wt)
  float cosv; //cos(w)
  float sinv; //sin(w)
} oscillator_t;

typedef struct paramsStruct{
  //model parameters
  float amp;
  float omega;
  float force;
  float gam;
  float Dg;
  float Dp;
  float lambda;
  float dt;

  //simulation
  ulong periods;
  ulong spp;
  ulong steps;
  ulong trans;
  unsigned int paths;

  //flags
  bool secondOrder;
  bool compFlag;
  int mode;

  //constants
  float twoPi;      // 2.0f * Pi
  float diff2;      // sqrtf(6.0f*m.gam*m.Dg*m.dt)
  float diff1;      // sqrtf(2.0f*m.gam*m.Dg*m.dt)
  float comp;       //sqrtf(m.Dp*m.lambda)*m.dt;
  float ampmean;    //sqrtf(m.lambda/m.Dp)
  float mu;         // m.lambda * m.dt
  float halfStep;   // dt * 0.5f
  float wdt;        //dt * m.omega;
} params_t;

typedef struct streamStruct{
  //moment specific variables
  float *sv;              //v first moment
  float *sv2;             //v second moment
  float *d;               //diffusion

  //trajectory specific variables
  float *x;               //position array
  float *v;               //velocity array

  //last values of position, velosity, oscillation and RNG state for each particle
  float *lx;              //last x values
  float *lv;              //last v values
  oscillator_t *w;        //oscillaror array struct
  curandState *states;    //curandStates array
  unsigned int *seeds;    //seeds array

  //array sizes
  ulong paths;            //number of particles
  size_t size_m;          //moments output
  size_t size_f;          //position and velocity
  size_t size_lf;         //last values
  size_t size_s;          //RNG state
  size_t size_w;          //oscillations
  size_t size_ui;         //seeds

  //stream launch specific variables
  cudaStream_t id;        //cuda stream
  dim3 blocks;            //number of blocks
  dim3 threads;           //number of threads
} stream_t;

typedef struct deviceStruct{
  int id;
  stream_t stream[STREAMS];
} device_t;

typedef struct hostStruct{
  size_t size_f;
  size_t size_m;
  size_t size_ui;
  unsigned int *seeds;
  float *x;
  float *v;
  float *sv;
  float *sv2;
  float *d;
} host_t;

#endif //__BROWN_H__
