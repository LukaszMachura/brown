#ifndef __TRAJECTORYKERNEL_H__
#define __TRAJECTORYKERNEL_H__

//#define _GNU_SOURCE
#include <math.h>

#include <curand.h>
#include <curand_kernel.h>

#include "help_functions.h"
#include "brown_types.h"
#include "algorithm.h"
#include "cudaSafe.h"

#ifdef __cplusplus
extern "C" {
#endif

void trajRun(device_t (*devs)[DEVS]);


#ifdef __cplusplus
}
#endif

#endif //__TRAJECTORYKERNEL_H__
