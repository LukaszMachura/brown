#ifndef __MOMENTS_H__
#define __MOMENTS_H__

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "cudaSafe.h"
#include "help_functions.h"

#ifdef __cplusplus
extern "C" {
#endif

void momentsAlloc(stream_t *stream);
void momentsMemcpy(const device_t (*devs)[DEVS], host_t *h, float *sv, float *sv2, float *d);

#ifdef __cplusplus
}
#endif

#endif
