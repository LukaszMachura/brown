#ifndef __INIT_H__
#define __INIT_H__

#include <curand.h>
#include <curand_kernel.h>

#include "help_functions.h"
#include "brown_types.h"
#include "rng.h"
#include "cudaSafe.h"
#include "trajectory.h"
#include "moments.h"

#ifdef __cplusplus
extern "C" {
#endif

void streamAlloc(stream_t *stream, const ulong paths, const int mode);
void devsAlloc(device_t (*devs)[DEVS], const ulong paths, const int mode);

#ifdef __cplusplus
}
#endif

#endif
