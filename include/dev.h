#ifndef __DEV_H__
#define __DEV_H__

#include <curand.h>
#include <curand_kernel.h>

#include "brown_types.h"
#include "cudaSafe.h"
#include "init.h"

#ifdef __cplusplus
extern "C" {
#endif

void devsRestore(const device_t (*devsHost)[DEVS], device_t (*devs)[DEVS]);
void devsFree(device_t (*devs)[DEVS]);

#ifdef __cplusplus
}
#endif

#endif
