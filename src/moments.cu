#include "moments.h"

void momentsAlloc(stream_t *stream)
{
  stream->size_m = stream->blocks.x * sizeof(float);

  //allocate device memory for x, v, w arrays
  gpuErrchk(cudaMalloc((void**)&stream->sv, stream->size_m));
  gpuErrchk(cudaMalloc((void**)&stream->sv2, stream->size_m));
  gpuErrchk(cudaMalloc((void**)&stream->d, stream->size_m));
}

static void momentsMemcpyStream(const stream_t *stream, host_t *h, const int idx)
{
  ulong stride = idx * stream->blocks.x;

  gpuErrchk(cudaMemcpyAsync(h->sv + stride, stream->sv, stream->size_m, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(h->sv2 + stride, stream->sv2, stream->size_m, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(h->d + stride, stream->d, stream->size_m, cudaMemcpyDeviceToHost, stream->id));
}

/* copy x,v array from device */
static void momentsMemcpyDev(const device_t *dev, host_t *h)
{
  gpuErrchk(cudaSetDevice(dev->id));
  int idx = dev->id * STREAMS;
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    momentsMemcpyStream(&dev->stream[i], h, idx++);
  }
}

void momentsMemcpy(const device_t (*devs)[DEVS], host_t *h, float *sv, float *sv2, float *d)
{
  int i;
  (*sv) = 0;
  (*sv2) = 0;
  (*d) = 0;

  #pragma unroll DEVS
  for (i = 0; i < DEVS; i++){
    momentsMemcpyDev(&(*devs)[i], h);
  }

  devsSynchronize();

  int n = h->size_m / sizeof(float);

  for (i = 0; i < n; i++){
    (*sv) += h->sv[i];
    (*sv2) += h->sv2[i];
    (*d) += h->d[i];
  }
  (*sv) /= n;
  (*sv2) /= n;
  (*d) /= n;
}
