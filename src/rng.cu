#include "rng.h"

/******************************** KERNEL **************************************/
__global__ void rngInitKernel(stream_t stream, const ulong i)
{
  const ulong tid = threadIdx.x + blockIdx.x * blockDim.x; //thread index

  //cache seeds and states in local variables
  unsigned int seed = stream.seeds[tid];
  curandState state;

  #ifndef TEST
    curand_init(seed, tid + i, 0, &state);
  #else
    curand_init(tid + i, tid + i, 0, &state);
  #endif

  stream.states[tid] = state;
}
/******************************************************************************/


/*init random generator states assuming device context is set,
returns pointer to device memory of type curandState * */
static __host__ void rngInitStream(stream_t *stream, const int idx)
{
  ulong stride = idx * stream->blocks.x * stream->threads.x;

  //initialize states on device
  rngInitKernel<<<stream->blocks, stream->threads>>>((*stream), stride);
}

static void rngInitDev(device_t *dev)
{
  int idx = dev->id * STREAMS;
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    rngInitStream(&dev->stream[i], idx++);
  }
}

void rngInit(device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    rngInitDev(&(*devs)[i]);
  }
}

/********************* SEEDS GENERATION ON HOST *******************************/
void seedsGen(host_t *host, const ulong paths)
{
  curandGenerator_t gen;

  //output array size
  host->size_ui = paths * sizeof(unsigned int);

  //allocate page-locked and write-combined host memory
  gpuErrchk(cudaHostAlloc((void**)&host->seeds, host->size_ui, cudaHostAllocPortable | cudaHostAllocWriteCombined));

  //create generator
  curandErrchk(curandCreateGeneratorHost(&gen, CURAND_RNG_PSEUDO_DEFAULT));
  curandErrchk(curandSetPseudoRandomGeneratorSeed(gen, time(NULL)));

  //generate seeds
  curandErrchk(curandGenerate(gen, host->seeds, paths));

  //destroy generator
  curandErrchk(curandDestroyGenerator(gen));
}
/******************************************************************************/


/**************************** SEEDS MEMCPY ************************************/
static void seedsMemcpyStream(const host_t *h, stream_t *stream, const int idx)
{
  ulong stride = idx * stream->blocks.x * stream->threads.x;
  gpuErrchk(cudaMemcpyAsync(stream->seeds, h->seeds + stride, stream->size_ui,
                            cudaMemcpyHostToDevice, stream->id));
}

/* copy x,v array from device */
static void seedsMemcpyDev(const host_t *h, device_t *dev)
{
  int idx = dev->id * STREAMS;
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    seedsMemcpyStream(h, &dev->stream[i], idx++);
  }
}

void seedsMemcpy(const host_t *h, device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    seedsMemcpyDev(h, &(*devs)[i]);
  }
}
/******************************************************************************/


/**************************** SEEDS ALLOCATION ********************************/
void seedsAlloc(stream_t *stream)
{
  stream->size_ui = stream->blocks.x * stream->threads.x * sizeof(unsigned int);
  gpuErrchk(cudaMalloc((void**)&stream->seeds, stream->size_ui));
}
/******************************************************************************/


/*************************** SEEDS DEALLOCATION *******************************/
void seedsFreeHost(host_t *h){
  if (h->size_ui != 0){
    h->size_ui = 0;
    gpuErrchk(cudaFreeHost(h->seeds));
  }
}
/******************************************************************************/
