#include "host.h"

/**************************** HOST MEMORY ALLOCATION **************************/
/* host memory initialization */
void hostAlloc(host_t *h, const ulong paths, const int mode)
{
  h->size_f = 0;
  h->size_m = 0;
  h->size_ui = 0;

  if (mode == TRAJECTORY){
    h->size_f = STEPS * paths * sizeof(float); //2d flatten array size [paths, STEPS]

    //allocate page-locked host memory for output arrays x, v
    gpuErrchk(cudaHostAlloc((void**)&h->x, h->size_f, cudaHostAllocPortable));
    gpuErrchk(cudaHostAlloc((void**)&h->v, h->size_f, cudaHostAllocPortable));
  } else if (mode == MOMENTS) {
    h->size_m = paths / THREADS * sizeof(float);

    gpuErrchk(cudaHostAlloc((void**)&h->sv, h->size_m, cudaHostAllocPortable));
    gpuErrchk(cudaHostAlloc((void**)&h->sv2, h->size_m, cudaHostAllocPortable));
    gpuErrchk(cudaHostAlloc((void**)&h->d, h->size_m, cudaHostAllocPortable));
  }
}
/******************************************************************************/


/***************************** HOST FREE MEMORY *******************************/
/* free output arrays */
void hostFree(host_t *h)
{
  if (h->size_ui != 0){
    gpuErrchk(cudaFreeHost(h->seeds));
  }
  if (h->size_f != 0){
    h->size_f = 0;
    gpuErrchk(cudaFreeHost(h->x));
    gpuErrchk(cudaFreeHost(h->v));
  } else if (h->size_m != 0){
    h->size_m = 0;
    gpuErrchk(cudaFreeHost(h->sv));
    gpuErrchk(cudaFreeHost(h->sv2));
    gpuErrchk(cudaFreeHost(h->d));
  }
}

/******************************************************************************/


/********************* STREAM HOST MEMORY ALLOCATION **************************/
static void streamHostAlloc(stream_t *stream, const ulong paths)
{
  streamSetParams(stream, paths);
  gpuErrchk(cudaHostAlloc((void**)&stream->lx, stream->size_lf, cudaHostAllocPortable | cudaHostAllocWriteCombined));
  gpuErrchk(cudaHostAlloc((void**)&stream->lv, stream->size_lf, cudaHostAllocPortable | cudaHostAllocWriteCombined));
  gpuErrchk(cudaHostAlloc((void**)&stream->w, stream->size_w, cudaHostAllocPortable | cudaHostAllocWriteCombined));
  gpuErrchk(cudaHostAlloc((void**)&stream->states, stream->size_s, cudaHostAllocPortable | cudaHostAllocWriteCombined));
}

/* Device memory allocation */
static void devHostAlloc(device_t *dev, const ulong paths)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamHostAlloc(&dev->stream[i], paths);
  }
}


void devsHostAlloc(device_t (*devs)[DEVS], const ulong paths)
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devHostAlloc(&(*devs)[i], paths);
  }
}
/******************************************************************************/


/*********************** STREAM FREE HOST MEMORY ******************************/
/* Free backed up stream memory on host */
static void streamFreeHost(stream_t *stream)
{
  gpuErrchk(cudaFreeHost(stream->lx));
  gpuErrchk(cudaFreeHost(stream->lv));
  gpuErrchk(cudaFreeHost(stream->w));
  gpuErrchk(cudaFreeHost(stream->states));
}

/* free backed up device memory on host */
static void devFreeHost(device_t *dev)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamFreeHost(&dev->stream[i]);
  }
}

/* free backed up devices memory on host */
void devsFreeHost(device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devFreeHost(&(*devs)[i]);
  }
}
/******************************************************************************/


/********************* BACKUP STREAM DEV MEMOMORY ON HOST *********************/
/* Backup stream arrays size, last x, v, state, w values
 * and stream launch params on host
 */
static void streamBackup(const stream_t *stream, stream_t *streamHost)
{
  //copy x, v, w, state last values from dev
  gpuErrchk(cudaMemcpyAsync(streamHost->lx, stream->lx, stream->size_lf, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(streamHost->lv, stream->lv, stream->size_lf, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(streamHost->w, stream->w, stream->size_w, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(streamHost->states, stream->states, stream->size_s, cudaMemcpyDeviceToHost, stream->id));
}

/* Backup device on host */
static void devBackup(const device_t *dev, device_t *devHost)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamBackup(&dev->stream[i], &devHost->stream[i]);
  }
}

/* backup devices on host */
void devsBackup(const device_t (*devs)[DEVS], device_t (*devsHost)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devBackup(&(*devs)[i], &(*devsHost)[i]);
  }
}
/******************************************************************************/
