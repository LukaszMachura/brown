#include "help_functions.h"

extern __constant__ params_t d_p;

/* constant memory initialization */
void constantsInit(params_t *p)
{
  if (p->mode == TRAJECTORY){
    //make all steps multiple of STEPS
    p->periods = (((p->periods * p->spp + STEPS - 1) / STEPS) * STEPS) / p->spp;
  }
  p->dt = 2.0f * PI / p->omega / p->spp; //step size
  p->steps = 1;

  //set constant values
  p->twoPi = 2.0f * PI;
  p->diff2 = sqrtf(6.0f * p->gam * p->Dg * p->dt);
  p->diff1 = sqrtf(2.0f * p->gam * p->Dg * p->dt);
  p->comp = sqrtf(p->Dp * p->lambda) * p->dt;
  p->ampmean = sqrtf(p->lambda / p->Dp);
  p->mu =  p->lambda * p->dt;
  p->halfStep = p->dt * 0.5f;
  p->wdt = p->dt * p->omega;
}

void constantsPin(params_t *p)
{
  gpuErrchk(cudaHostRegister((void*) p, sizeof(params_t), cudaHostRegisterPortable));
}

void constantsUnpin(params_t *p)
{
  gpuErrchk(cudaHostUnregister((void*) p));
}

/* device synchronization after async calls */
void devsSynchronize()
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    gpuErrchk(cudaDeviceSynchronize());
  }
}

/* Async copy params_t struct to all CUDA contexts */
void constantsMemcpy(const device_t (*devs)[DEVS], params_t *p)
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    gpuErrchk(cudaMemcpyToSymbolAsync(d_p, (void*) p, sizeof(params_t), 0, cudaMemcpyHostToDevice, (*devs)[i].stream[0].id));
  }
}

void streamSetParams(stream_t *stream, const ulong paths)
{
  stream->paths = paths;
  //grid size
  dim3 blocks(paths / THREADS / STREAMS / DEVS, 1, 1);
  dim3 threads(THREADS, 1, 1);
  stream->blocks = blocks;
  stream->threads = threads;

  //size of arrays
  stream->size_m = 0;
  stream->size_f = 0;
  stream->size_ui = 0;
  stream->size_lf = stream->blocks.x * stream->threads.x * sizeof(float);
  stream->size_w = stream->blocks.x * stream->threads.x * sizeof(oscillator_t);
  stream->size_s = stream->blocks.x * stream->threads.x * sizeof(curandState);
}
