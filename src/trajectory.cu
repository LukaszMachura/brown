#include "trajectory.h"

void trajAlloc(stream_t *stream)
{
  stream->size_f = stream->blocks.x * stream->threads.x * STEPS * sizeof(float);

  //allocate device memory for x, v, w arrays
  gpuErrchk(cudaMalloc((void**)&stream->x, stream->size_f));
  gpuErrchk(cudaMalloc((void**)&stream->v, stream->size_f));
}

static void trajMemcpyStream(const stream_t *stream, host_t *h, const int idx)
{
  ulong stride = idx * stream->blocks.x * stream->threads.x * STEPS;
  gpuErrchk(cudaMemcpyAsync(h->x + stride, stream->x, stream->size_f, cudaMemcpyDeviceToHost, stream->id));
  gpuErrchk(cudaMemcpyAsync(h->v + stride, stream->v, stream->size_f, cudaMemcpyDeviceToHost, stream->id));
}

/* copy x,v array from device */
static void trajMemcpyDev(const device_t *dev, host_t *h)
{
  int idx = dev->id * STREAMS;
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    trajMemcpyStream(&dev->stream[i], h, idx++);
  }
}

void trajMemcpy(const device_t (*devs)[DEVS], host_t *h)
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    trajMemcpyDev(&(*devs)[i], h);
  }
}
