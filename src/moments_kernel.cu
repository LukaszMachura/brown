#include "moments_kernel.h"

//particle constant memory
extern __constant__ params_t d_p;

__global__ void momentsKernel(stream_t stream)
{
  __shared__ float sv[THREADS], sv2[THREADS], d[THREADS];
  const ulong tid = threadIdx.x + blockIdx.x * blockDim.x; //thread index

  //store constant params in local variables
  const params_t p = d_p;

  //cache last x, v, state and oscillator struct
  float x = stream.lx[tid];
  float v = stream.lv[tid];
  oscillator_t w = stream.w[tid];
  curandState state = stream.states[tid];

  float lsv = 0, lsv2 = 0, ld = 0;

  //jump countdown
  int pcd = 0;
  if (p.secondOrder){
    int pcd = (int) floor(-logf(curand_uniform(&state)) / p.mu + 0.5f);
  }

  for (ulong sid = 0; sid < p.spp * p.periods; sid++){
    makeStep(&pcd, &state, &x, &v, &w, &p); //calculate next step

    if (sid > p.spp * p.trans){
      lsv += v;
      lsv2 += v * v;
    }
  }

  ld = (x * x - x) / (2.0f * p.periods * p.twoPi / p.omega);
  __syncthreads();
  sv[threadIdx.x] = lsv;
  sv2[threadIdx.x] = lsv2;
  d[threadIdx.x] = ld;

  int i = THREADS / 2;
  __syncthreads();
  while (i != 0){
    if (threadIdx.x < i){
      sv[threadIdx.x] += sv[threadIdx.x + i];
      sv2[threadIdx.x] += sv2[threadIdx.x + i];
      d[threadIdx.x] += d[threadIdx.x + i];
    }
    __syncthreads();
    i /= 2;
  }

  if (threadIdx.x == 0){
    sv[threadIdx.x] /= (THREADS * (p.spp * (p.periods - p.trans)));
    sv2[threadIdx.x] /= (THREADS * (p.spp * (p.periods - p.trans)));
    d[threadIdx.x] /= THREADS;

    stream.sv[blockIdx.x] = sv[threadIdx.x];
    stream.sv2[blockIdx.x] = sv2[threadIdx.x];
    stream.d[blockIdx.x] = d[threadIdx.x];
  }
  __syncthreads();
}

static __host__ void momentsRunStream(stream_t *stream)
{
  momentsKernel<<< stream->blocks, stream->threads, 0, stream->id>>>((*stream));
}

static __host__ void momentsRunDev(device_t *dev)
{
  gpuErrchk(cudaSetDevice(dev->id));
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    momentsRunStream(&dev->stream[i]);
  }
}

__host__ void momentsRun(device_t (*dev)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    momentsRunDev(&(*dev)[i]);
  }
}
