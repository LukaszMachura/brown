#include "init_kernel.h"

/* device constant memory declaration */
extern __constant__ params_t d_p;

/* oscillator initialization */
__device__ oscillator_t init_w(curandState *state, const float wdt){
  float w0 = curand_uniform(state) * 2.0f * PI;  //w0 in [0, 2 * pi]
  oscillator_t w;
  sincosf(wdt, &w.sint, &w.cost); //calculate step
  sincosf(w0, &w.sinv, &w.cosv);  //calculate start value

  return w;
}

/* particles zeroth step initialization */
__global__ void initKernel(stream_t stream)
{
  const ulong tid = threadIdx.x + blockIdx.x * blockDim.x; //thread index

  //cache state and constants struct at local memory
  curandState state = stream.states[tid];
  const params_t p = d_p;

  //initialization of zeroth
  stream.lx[tid] = curand_uniform(&state); //x in [0,1]
  stream.lv[tid] = curand_uniform(&state) * 4.0f - 2.0f; //v in [-2,2]
  stream.w[tid] = init_w(&state, p.wdt); //init oscillator struct
  stream.states[tid] = state; //write state back to global memory
}


static __host__ void initStream(stream_t *stream)
{
  initKernel<<< stream->blocks, stream->threads, 0, stream->id>>>((*stream));
}

/* run init for each device stream */
static __host__ void initDev(device_t *dev)
{
  gpuErrchk(cudaSetDevice(dev->id));
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    initStream(&dev->stream[i]);
  }
}

/* run init for all devices */
__host__ void init(device_t (*devs)[DEVS])
{
  rngInit(devs);
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    initDev(&(*devs)[i]);
  }
}
