#include "init.h"

/* Allocation of arrays for saving last values of  position, velocity
 * oscillator struct and RNG state allocation.
 *
 * Calculation of kernel launch parameters and size of arrays.
 *
 * Initialization of RNG states in device.
 */
void streamAlloc(stream_t *stream, const ulong paths, const int mode)
{
  streamSetParams(stream, paths);

  //create stream
  gpuErrchk(cudaStreamCreate(&stream->id));

  //allocate memory for last values of position, velocity and oscillator struct
  gpuErrchk(cudaMalloc((void**)&stream->lx, stream->size_lf));
  gpuErrchk(cudaMalloc((void**)&stream->lv, stream->size_lf));
  gpuErrchk(cudaMalloc((void**)&stream->w, stream->size_w));
  gpuErrchk(cudaMalloc((void**)&stream->states, stream->size_s));

  if ((mode & MOMENTS) == MOMENTS){
    momentsAlloc(stream);
  }
  if ((mode & TRAJECTORY) == TRAJECTORY){
    trajAlloc(stream);
  }
  if ((mode & INIT) == INIT){
    seedsAlloc(stream);
  }
}


/* Device memory allocation */
static void devAlloc(device_t *dev, const ulong paths, const int mode)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamAlloc(&dev->stream[i], paths, mode);
  }
}


void devsAlloc(device_t (*devs)[DEVS], const ulong paths, const int mode)
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devAlloc(&(*devs)[i], paths, mode);
  }
}
