#include "dev.h"

/***************************** STREAM RESTORE *********************************/
/* Restore stream last position, velosity, oscillator values and RNG state
 * Function assumes, that stream has been already allocated on device
 * Function works asynchronously
 */
static void streamRestore(const stream_t *streamHost, stream_t *stream)
{
  gpuErrchk(cudaMemcpyAsync(stream->lx, streamHost->lx, stream->size_lf, cudaMemcpyHostToDevice, stream->id));
  gpuErrchk(cudaMemcpyAsync(stream->lv, streamHost->lv, stream->size_lf, cudaMemcpyHostToDevice, stream->id));
  gpuErrchk(cudaMemcpyAsync(stream->w, streamHost->w, stream->size_w, cudaMemcpyHostToDevice, stream->id));
  gpuErrchk(cudaMemcpyAsync(stream->states, streamHost->states, stream->size_s, cudaMemcpyHostToDevice, stream->id));
}


/* Restore device's streams from backup */
static void devRestore(const device_t *devHost, device_t *dev)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamRestore(&devHost->stream[i], &dev->stream[i]);
  }
}

/* Restore each device */
void devsRestore(const device_t (*devsHost)[DEVS], device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devRestore(&(*devsHost)[i], &(*devs)[i]);
  }
}

/******************************************************************************/

/*************************** FREE STREAM MEMORY *******************************/
/* Free stream memory on device */
static void streamFree(stream_t *stream){
  gpuErrchk(cudaFree(stream->lx));
  gpuErrchk(cudaFree(stream->lv));
  gpuErrchk(cudaFree(stream->w));
  gpuErrchk(cudaFree(stream->states));
  if (stream->size_ui != 0){
    stream->size_ui = 0;
    gpuErrchk(cudaFree(stream->seeds));
  }
  if (stream->size_f != 0){
    stream->size_f = 0;
    gpuErrchk(cudaFree(stream->x));
    gpuErrchk(cudaFree(stream->v));
  } else if (stream->size_m != 0){
    stream->size_m = 0;
    gpuErrchk(cudaFree(stream->sv));
    gpuErrchk(cudaFree(stream->sv2));
    gpuErrchk(cudaFree(stream->d));
  }

  gpuErrchk(cudaStreamDestroy(stream->id));
}


/* free device memory */
static void devFree(device_t *dev)
{
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    streamFree(&dev->stream[i]);
  }
}


/* free devices memory*/
void devsFree(device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    gpuErrchk(cudaSetDevice(i));
    devFree(&(*devs)[i]);
  }
}
/******************************************************************************/
