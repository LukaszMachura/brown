#include "trajectory_kernel.h"

//particle constant memory
__constant__ params_t d_p;

__global__ void trajKernel(stream_t stream)
{
  const ulong tid = threadIdx.x + blockIdx.x * blockDim.x; //thread index
  const ulong pid = tid * STEPS; // particle first step index
  ulong sid; //step index

  //store constant params in local variables
  const params_t p = d_p;

  //cache last x, v, state and oscillator struct
  float x = stream.lx[tid];
  float v = stream.lv[tid];
  oscillator_t w = stream.w[tid];
  curandState state = stream.states[tid];

  //jump countdown
  int pcd = 0;
  if (p.secondOrder){
    int pcd = (int) floor(-logf(curand_uniform(&state)) / p.mu + 0.5f);
  }

  for (sid = 0; sid < STEPS; sid++){
    makeStep(&pcd, &state, &x, &v, &w, &p); //calculate next step
    //save step at global mem
    stream.x[pid + sid] = x;
    stream.v[pid + sid] = v;
  }

  //write last x, v, state and oscillator struct back to global memory
  stream.lx[tid] = x;
  stream.lv[tid] = v;
  stream.w[tid] = w;
  stream.states[tid] = state;
}


__host__ static void trajRunStream(stream_t *stream)
{
  trajKernel<<< stream->blocks, stream->threads, 0, stream->id>>>((*stream));
}

__host__ static void trajRunDev(device_t *dev)
{
  gpuErrchk(cudaSetDevice(dev->id));
  #pragma unroll STREAMS
  for (int i = 0; i < STREAMS; i++){
    trajRunStream(&dev->stream[i]);
  }
}

__host__ void trajRun(device_t (*devs)[DEVS])
{
  #pragma unroll DEVS
  for (int i = 0; i < DEVS; i++){
    trajRunDev(&(*devs)[i]);
  }
}
