#include "brownlib.h"
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

void help(){
  printf("help\n");
  exit(0);
}

//getopt options
static struct option options[] = {
    {"amp", required_argument, NULL, 'a'},
    {"omega", required_argument, NULL, 'b'},
    {"force", required_argument, NULL, 'c'},
    {"gam", required_argument, NULL, 'd'},
    {"Dg", required_argument, NULL, 'e'},
    {"Dp", required_argument, NULL, 'f'},
    {"lambda", required_argument, NULL, 'g'},
    {"help", no_argument, NULL, 'h'},
    {"comp", required_argument, NULL, 'i'},
    {"algorithm", required_argument, NULL, 'j'},
    {"paths", required_argument, NULL, 'k'},
    {"periods", required_argument, NULL, 'l'},
    {"spp", required_argument, NULL, 'n'},
    {"output", required_argument, NULL, 'o'},
    {"mode", required_argument, NULL, 'p'},
    {"trans", required_argument, NULL, 'q'},
    {NULL, 0, NULL, 0}
};

static void parse_cla(int argc, char **argv, int *mode, params_t *p, FILE **fp, int *outFlag)
{
  int op;

  //output flag default value
  (*outFlag) = -1;
  p->trans = 0;

  //parce command line arguments
  while( (op = getopt_long(argc, argv, "ha:b:c:d:e:f:g:i:j:k:l:n:o:p:q", options, NULL)) != EOF) {
    switch (op) {
      case 'a':
          p->amp = atof(optarg);
          break;
      case 'b':
          p->omega = atof(optarg);
          break;
      case 'c':
          p->force = atof(optarg);
          break;
      case 'd':
          p->gam = atof(optarg);
          break;
      case 'e':
          p->Dg = atof(optarg);
          break;
      case 'f':
          p->Dp = atof(optarg);
          break;
      case 'g':
          p->lambda = atof(optarg);
          break;
      case 'h':
          help();
          break;
      case 'i':
          p->compFlag = atoi(optarg);
          break;
      case 'j':
          if ( !strcmp(optarg, "predcorr") ){
              p->secondOrder = 1;
          }else if ( !strcmp(optarg, "euler") ){
              p->secondOrder = 0;
          }
          break;
      case 'k':
          p->paths = atol(optarg);
          p->paths = ((p->paths + THREADS - 1) / THREADS) * THREADS;
          break;
      case 'l':
          p->periods = atol(optarg);
          break;
      case 'n':
          p->spp = atol(optarg);
          break;
      case 'o':
          if (!strcmp(optarg, "stdout")){
            (*outFlag) = 0;
          } else {
            remove(optarg);
            (*fp) = fopen(optarg,"w");
            if ((*fp) == NULL){
              fprintf(stderr,"Can't open a file %s\n", optarg);
            } else {
              (*outFlag) = 1;
              fprintf(stdout, "Writing to file %s\n", optarg);
            }
          }
          break;
      case 'p':
          if ( !strcmp(optarg, "trajectory") ) {
              (*mode) = 1;
              p->mode = TRAJECTORY;
              p->steps = 1;
          }
          break;
      case 'q':
        p->trans = atoi(optarg);
        break;
    }
  }
}

static void print_traj(const host_t *h, const params_t *p,
                     FILE **fp, const int outFlag)
{
  static float t = 0;
  ulong pid, sid, idx;
  for (sid = 0; sid < STEPS; sid++){
    for (pid = 0; pid < p->paths; pid++){
      idx = sid + pid * STEPS;
      if (outFlag == 1){
        fprintf((*fp), "%e %e %e %lu\n", t, h->x[idx], h->v[idx], pid);
      } else if (outFlag == 0){
        fprintf(stdout, "%e %e %e %lu\n", t, h->x[idx], h->x[idx], pid);
      }
    }
    t += p->dt;
  }
}

/* print header of csv file */
void print_header(FILE **fp, const int outFlag)
{
  if (outFlag == 1){
    fprintf((*fp), "#t x v path\n");
  } else if (outFlag == 0){
    fprintf(stdout, "#t x v path\n");
  }
}

void trajectory(params_t *p, FILE **fp, const int outFlag)
{
  print_header(fp, outFlag);
  device_t devs[DEVS];
  host_t h;

  hostAlloc(&h, p->paths, TRAJECTORY);
  seedsGen(&h, p->paths);
  devsAlloc(&devs, p->paths, INIT | TRAJECTORY);

  constantsInit(p);
  constantsPin(p);

  constantsMemcpy(&devs, p);
  seedsMemcpy(&h, &devs);

  rngInit(&devs);
  init(&devs);

  ulong kernelRuns = p->periods * p->spp / STEPS;
  for (ulong run = 0; run < kernelRuns; run++){
    trajRun(&devs);
    trajMemcpy(&devs, &h);
    devsSynchronize(&devs);
    print_traj(&h, p, fp, outFlag);
  }
  if (outFlag == 1){
    fclose((*fp));
  }

  devsFree(&devs);
  hostFree(&h);
}

int main(int argc, char **argv){
  FILE *fp = NULL;
  int outFlag;
  int mode = -1;
  params_t p;

  parse_cla(argc, argv,&mode, &p, &fp, &outFlag);
  if (mode == 1){
    trajectory(&p, &fp, outFlag);
  }
}
