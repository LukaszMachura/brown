#include "algorithm.h"

__device__ void calcNextW(oscillator_t *w)
{
  float ns, nc;

  //calculate cos(w_n + w * dt) and sin(w_n + w * dt)
  ns = w->sint * w->cosv + w->cost * w->sinv;
  nc = w->cost * w->cosv - w->sint * w->sinv;

  w->sinv = ns;
  w->cosv = nc;
}

//reduce periodic variable to the base domain
__device__ void fold(float *nx, float *xfc)
{
  if (fabs((*nx)) > 1.0f){
    float temp = floor((*nx));

    (*nx) -= temp;
    (*xfc) += temp;
  }
}

__device__ float drift(const float x,const float v, const float cosw, const params_t *p)
{
  return -p->gam * v - p->twoPi * cosf(p->twoPi * x) + p->amp * cosw + p->force;
}

__device__ float secondOrderDiffusion(curandState * const state, const float Dg,
                 const float diff2)
{
  if (Dg != 0.0f) {
    const float r = curand_uniform(state);
    if ( r <= 1.0f / 6 ) {
      return -diff2;
    } else if ( r > 1.0f / 6 && r <= 2.0f / 6 ) {
      return diff2;
    } else {
      return 0.0f;
    }
  } else {
    return 0.0f;
  }
}

__device__ float firstOrderDiffusion(curandState * const state, const float Dg,
                 const float diff1)
{
  if (Dg != 0.0f) {
    const float r = curand_uniform(state);
    if ( r <= 0.5f ) {
      return -diff1;
    } else {
      return diff1;
    }
  } else {
    return 0.0f;
  }
}

__device__ float adapted_jump(int *pcd, curandState * const state,
                 const params_t *p)
{
  if (p->Dp != 0.0f) {
    if ((*pcd) <= 0) {
      (*pcd) = (int) floor(-logf(curand_uniform(state)) / p->mu + 0.5f);

      if (p->compFlag) {
        return -logf(curand_uniform(state)) / p->ampmean - p->comp;
      } else {
        return -logf(curand_uniform(state)) / p->ampmean;
      }
    } else {
      (*pcd)--;
      if (p->compFlag) {
        return -p->comp;
      } else {
        return 0.0f;
      }
    }
  } else {
    return 0.0f;
  }
}

__device__ float regular_jump(curandState * const state, const params_t *p)
{
  if (p->Dp != 0.0f) {
    const unsigned int n = curand_poisson(state, p->mu);
    float s = 0.0f;
    int i;

    for (i = 0; i < n; i++) {
      s += -logf(curand_uniform(state)) / p->ampmean;
    }

    if (p->compFlag){
      s -= p->comp;
    }

    return s;
  } else {
    return 0.0f;
  }
}

/* simplified weak order 2.0 adapted predictor-corrector scheme
( see E. Platen, N. Bruti-Liberati; Numerical Solution of Stochastic Differential
  Equations with Jumps in Finance; Springer 2010; p. 503, p. 532 )
*/
__device__ void predcorr(int *pcd, curandState * const state, float *x, float *v,
                oscillator_t *w, const params_t *p)
{
  float l_xt, l_xtt, predl_x;
  float l_vt, l_vtt, predl_v;

  l_xt = (*v);
  l_vt = drift((*x), (*v), w->cosv, p);
  calcNextW(w);

  predl_x = (*x) + l_xt * p->dt;
  predl_v = (*v) + l_vt * p->dt + secondOrderDiffusion(state, p->Dg, p->diff2);

  l_xtt = predl_v;
  l_vtt = drift(predl_x, predl_v, w->cosv, p);

  predl_x = (*x) + p->halfStep * (l_xt + l_xtt);
  predl_v = (*v) + p->halfStep * (l_vt + l_vtt) +
              secondOrderDiffusion(state, p->Dg, p->diff2);

  l_xtt = predl_v;
  l_vtt = drift(predl_x, predl_v, w->cosv, p);

  (*x) += p->halfStep * (l_xt + l_xtt);
  (*v) += p->halfStep * (l_vt + l_vtt) +
         secondOrderDiffusion(state, p->Dg, p->diff2) +
         adapted_jump(pcd, state, p);
}

/* simplified weak order 1.0 regular euler-maruyama scheme
( see E. Platen, N. Bruti-Liberati; Numerical Solution of Stochastic Differential
  Equations with Jumps in Finance; Springer 2010; p. 508,
  C. Kim, E. Lee, P. Talkner, and P.Hanggi; Phys. Rev. E 76; 011109; 2007 )
*/
__device__ void eulermaruyama(curandState * const state, float *x, float *v, oscillator_t *w,
                const params_t *p)
{
  float l_xt;
  float l_vt;

  l_xt = (*x) + (*v) * p->dt;
  l_vt = (*v) + drift((*x), (*v), w->cosv, p) * p->dt +
                 firstOrderDiffusion(state, p->Dg, p->diff1) +
                 regular_jump(state, p);
  calcNextW(w);

  (*x) = l_xt;
  (*v) = l_vt;
}

__device__ void makeStep(int *pcd, curandState * const state, float *x, float *v,
                oscillator_t *w, const params_t *p)
{
  float xfc;
  xfc = 0.0f;

  for (ulong i = 0; i < p->steps; i++){
    //fold path parameters
    fold(x, &xfc);

    //switch algorithm
    if (p->secondOrder){
      predcorr(pcd, state, x, v, w, p);
    } else {
      eulermaruyama(state, x, v, w, p);
    }
  }
  (*x) += xfc;
}
