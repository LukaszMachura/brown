from distutils.core import setup, Extension
import numpy as np

module1 = Extension('brown',
                    sources = ['py/brownmodule.c'],
                    include_dirs = ['.','include', np.get_include(), '/usr/local/cuda/include'],
                    library_dirs= ['/home/sektus/workspace/Projects/brown/brown/lib','/usr/local/cuda/lib64' ],
                    libraries=['brown', 'curand', 'cudart'])

setup (name = 'brown',
       version = '1.1',
       description = 'This is a demo package',
       ext_modules = [module1])
