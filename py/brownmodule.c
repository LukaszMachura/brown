#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>
#include <numpy/arrayobject.h>
#include "brownlib.h"

typedef struct{
  PyObject_HEAD
  params_t p;
  device_t devs[DEVS];
  device_t devsHost[DEVS];
  bool initialized;
} BrownObject;

static void Brown_dealloc(BrownObject *self)
{
  devsFreeHost(&self->devsHost); //free backed up device memory on host
  constantsUnpin(&self->p);
  Py_TYPE(self)->tp_free((PyObject *) self); //free Brown object
  print("self.__dealloc__()\n");
}

static PyObject *
Brown_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    BrownObject *self;
    self = (BrownObject *) type->tp_alloc(type, 0);
    self->initialized = 0;
    print("self.__new__()\n");
    return (PyObject *) self;
}

static int
Brown_init(BrownObject *self, PyObject *args, PyObject *kwds)
{
    static char *kwlist[] = {"amp", "omega", "force", "gam", "Dg", "Dp", "_lambda",
                "comp","paths", NULL};

    print("self.__init__(amp,omega,force,gam,Dg,Dp,_lambda,comp,paths)\n");
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "fffffffiI", kwlist,
                                     &self->p.amp, &self->p.omega, &self->p.force,
                                     &self->p.gam, &self->p.Dg, &self->p.Dp,
                                     &self->p.lambda, &self->p.comp, &self->p.paths))
        return -1;
    print("ptr = %p\n", &self->p);
    print("Constants pinning\n");
    constantsPin(&self->p);
    return 0;
}

static PyMemberDef Brown_members[] = {
    {NULL}  /* Sentinel */
};

static PyObject *
Brown_trajectory(BrownObject *self, PyObject *args, PyObject *kwds)
{
  static char *kwlist[] = {"periods", "spp", "algorithm", NULL};
  print("self.trajectory(periods, spp, algorithm)\n");
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "kki", kwlist, &self->p.periods,
                                   &self->p.spp, &self->p.secondOrder))
      return NULL;

  print("Host memory allocation\n");
  host_t h;
  hostAlloc(&h, self->p.paths, TRAJECTORY);

  print("Constants initialization\n");
  self->p.mode = TRAJECTORY;
  constantsInit(&self->p);

  print("Device memory allocation\n");
  if (!self->initialized){
    devsAlloc(&self->devs, self->p.paths, INIT | TRAJECTORY);

    print("Seeds generation\n");
    seedsGen(&h, self->p.paths);
    seedsMemcpy(&h, &self->devs);
  } else {
    devsAlloc(&self->devs, self->p.paths, TRAJECTORY);
  }

  print("Constants memcpy\n");
  constantsMemcpy(&self->devs, &self->p);

  if (!self->initialized){
    print("RNG and zeroth step initialization\n");
    rngInit(&self->devs);
    init(&self->devs);
    self->initialized = 1;
  } else {
    print("Restore device memory from backup\n");
    devsRestore(&self->devsHost, &self->devs);
  }


  print("Run simulation\n");
  trajRun(&self->devs);

  print("Copy results to host\n");
  trajMemcpy(&self->devs, &h);

  print("Allocate memory for stream backup on host\n");
  devsHostAlloc(&self->devsHost, self->p.paths);

  print("Backup device last values on host\n");
  devsBackup(&self->devs, &self->devsHost);

  print("Device synchronize\n");
  devsSynchronize();

  print("Free seeds host memory\n");
  seedsFreeHost(&h);

  print("Free device memory\n");
  devsFree(&self->devs);

  print("Creating output dict\n");

  //dimensions of output arrays
  long int *dims = malloc(sizeof(long int));
  dims[0] = self->p.spp * self->p.periods * self->p.paths;

  //create numpy array from output
  PyObject *x_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) h.x);
  PyObject *v_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) h.v);

  return Py_BuildValue("(OO)", x_arr, v_arr);
}

static PyObject *
Brown_moments(BrownObject *self, PyObject *args, PyObject *kwds)
{
  static char *kwlist[] = {"periods", "spp","trans","algorithm", NULL};
  print("Object.moments(periods, spp, algorithm)\n");
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "kkki", kwlist, &self->p.periods,
                                   &self->p.spp, &self->p.trans, &self->p.secondOrder))
      return NULL;

  print("Host memory allocation\n");
  host_t h;
  float sv, sv2, d;
  hostAlloc(&h, self->p.paths, MOMENTS);

  print("Constants initialization\n");
  constantsInit(&self->p);

  print("Device memory allocation\n");
  devsAlloc(&self->devs, self->p.paths, INIT | MOMENTS);

  print("Seeds generation\n");
  seedsGen(&h, self->p.paths);
  seedsMemcpy(&h, &self->devs);

  print("Constants memcpy\n");
  constantsMemcpy(&self->devs, &self->p);

  print("RNG and zeroth step initialization\n");
  rngInit(&self->devs);
  init(&self->devs);

  print("Run simulation\n");
  momentsRun(&self->devs);

  print("Copy results to host\n");
  momentsMemcpy(&self->devs, &h, &sv, &sv2, &d);

  print("Device synchronize\n");
  devsSynchronize();

  print("Free device memory\n");
  devsFree(&self->devs);

  print("Free host\n");
  hostFree(&h);

  return Py_BuildValue("(fff)", sv, sv2, d);
}

static PyObject *
Brown_moments1d(BrownObject *self, PyObject *args, PyObject *kwds)
{
  float begin, end, log;
  char domain;
  int points;

  static char *kwlist[] = {"periods", "spp","trans","algorithm","points", "begin", "end", "domain", "log", NULL};
  print("Object.moments(periods, spp, algorithm)\n");
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "kkkiiffCi", kwlist, &self->p.periods,
                                   &self->p.spp, &self->p.trans, &self->p.secondOrder,
                                   &points, &begin, &end, &domain, &log))
      return NULL;

  float sv, sv2, d;

  float dx = (end - begin) / points;

  float *xl = (float*)malloc(points * sizeof(float));
  float *svl = (float*)malloc(points * sizeof(float));
  float *sv2l = (float*)malloc(points * sizeof(float));
  float *dl = (float*)malloc(points * sizeof(float));

  params_t p = self->p;
  constantsPin(&p);
  p.mode = MOMENTS;
  float *x = &p.force;

  switch(domain) {
        case 'a':
            x = &p.amp;
            break;
        case 'w':
            x = &p.omega;
            break;
        case 'f':
            x = &p.force;
            break;
        case 'g':
            x = &p.gam;
            break;
        case 'D':
            x = &p.Dg;
            break;
        case 'p':
            x = &p.Dp;
            break;
        case 'l':
            x = &p.lambda;
            break;
    }
  (*x) = begin;

  print("Device memory allocation\n");
  devsAlloc(&self->devs, self->p.paths, INIT | MOMENTS);

  print("Allocate host memory for output arrays\n");
  host_t h;
  hostAlloc(&h, self->p.paths, MOMENTS);



  for (int i = 0; i < points; i++){

    print("Constants initialization\n");
    constantsInit(&p);

    print("Seeds generation\n");
    seedsGen(&h, self->p.paths);
    seedsMemcpy(&h, &self->devs);

    print("Constants memcpy\n");
    constantsMemcpy(&self->devs, &p);

    print("RNG and zeroth step initialization\n");
    rngInit(&self->devs);
    init(&self->devs);

    print("Run simulation\n");
    momentsRun(&self->devs);

    print("Copy results to host\n");
    momentsMemcpy(&self->devs, &h, &sv, &sv2, &d);

    print("Device synchronize\n");
    devsSynchronize();

    xl[i] = (*x);
    svl[i] = sv;
    sv2l[i] = sv2;
    dl[i] = d;

    (*x) += dx;
  }

  print("Free device memory\n");
  devsFree(&self->devs);

  print("Free host\n");
  hostFree(&h);

  long int *dims = malloc(sizeof(long int));
  dims[0] = points;

  //create numpy array from output
  PyObject *x_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) xl);
  PyObject *sv_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) svl);
  PyObject *sv2_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) sv2l);
  PyObject *d_arr = (PyObject *) PyArray_SimpleNewFromData(1, dims, NPY_FLOAT32, (void*) dl);

  return Py_BuildValue("(OOOO)", x_arr, sv_arr, sv2_arr, d_arr);
}


static PyMethodDef Brown_methods[] = {
  {"trajectory", (PyCFunction) Brown_trajectory, METH_VARARGS | METH_KEYWORDS, "return traj"},
  {"moments", (PyCFunction) Brown_moments, METH_VARARGS | METH_KEYWORDS, "return moments"},
  {"moments1d", (PyCFunction) Brown_moments1d, METH_VARARGS | METH_KEYWORDS, "return moments"},
  {NULL}
};

static PyTypeObject BrownType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "brown.Brown",
    .tp_doc = "Brown",
    .tp_basicsize = sizeof(BrownObject),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_new = Brown_new,
    .tp_init = (initproc) Brown_init,
    .tp_dealloc = (destructor) Brown_dealloc,
    .tp_members = Brown_members,
    .tp_methods = Brown_methods,
};

static PyModuleDef brownmodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "brown",
    .m_doc = "brown cuda model",
    .m_size = -1,
};


PyMODINIT_FUNC
PyInit_brown(void)
{
    import_array();
    PyObject *m;
    if (PyType_Ready(&BrownType) < 0)
        return NULL;

    m = PyModule_Create(&brownmodule);
    if (m == NULL)
        return NULL;

    PyModule_AddIntConstant(m, "STREAMS", STREAMS);
    PyModule_AddIntConstant(m, "THREADS", THREADS);
    PyModule_AddIntConstant(m, "STEPS", STEPS);
    PyModule_AddIntConstant(m, "DEVS", DEVS);

    Py_INCREF(&BrownType);
    if (PyModule_AddObject(m, "Brown", (PyObject *) &BrownType) < 0) {
        Py_DECREF(&BrownType);
        Py_DECREF(m);
        return NULL;
    }
    return m;
}
